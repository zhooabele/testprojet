console.log('hello depuis main');
const technosDiv = document.querySelector('#technos');

function loadTechnologies() {
    fetch('http://localhost:3002/technos')//rechercher API
        .then(response => {//promise return response
            response.json()// format en json
                .then(technos => {//promise return technose
                    const allTechnos = technos.map(t => `<div><b>${t.name}</b> ${t.description}  <a href="${t.url}">site de ${t.name}</a> </div>`)
                            .join('');//map chaque review et après join les ''

                    technosDiv.innerHTML = allTechnos;//afficher dans le calque
                });
        })
        .catch(console.error);//si une erreur est trouvé
}

loadTechnologies();//appel du fonction

// if('serviceworker' in navigator){
//
// }

if(navigator.serviceWorker){

    navigator.serviceWorker.register('/sw.js').catch(err =>console.err)
}
