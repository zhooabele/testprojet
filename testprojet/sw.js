self.addEventListener('install', evt =>{ //callback
  console.log('install',evt);
})

self.addEventListener('activate', evt =>{
  console.log('activate', evt);
})

self.addEventListener('fetch', evt =>{
    if(!navigator.onLine){
      const headers = { headers: {'Content-Type': 'text/html'}}
      evt.respondWith(new Response('<h1>Pas de connexion internet</h1>',headers))
    }
  console.log(evt.request.url);
})
